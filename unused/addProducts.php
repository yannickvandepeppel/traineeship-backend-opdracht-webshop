<?php
    $query = "INSERT INTO products (
            name,
            price,
            category,
            description,
            stock
            )
        VALUES
        ROW(
            'Electric Guitar',
            49999,
            'Guitars',
            'Classic guitar with rock sound.',
            1
        ),
        ROW(
            'Bass Guitar',
            59999,
            'Guitars',
            'Lowest member of the guitar family.',
            1
        ),
        ROW(
            'Trumpet',
            39999,
            'Brass',
            'Bright, loud instrument for the brazen few.',
            1
        ),
        ROW(
            'Trombone',
            49999,
            'Brass',
            'Good for accompanying your friends fails.',
            1
        ),
        ROW(
            'Triangle',
            99999,
            'Percussion',
            'The king of all instruments.',
            1
        )
    ";

    var_dump($query);

    if($mysqli->query($query) === TRUE)
    {
        echo "5 Products succesfully added\n";
    } 
    else
    {
        echo "Error creating table Products.\n".$mysqli->error;
    }
?>