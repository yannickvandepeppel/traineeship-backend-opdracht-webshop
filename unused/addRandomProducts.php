<?php
    $brands = array("Fender", "Gibson", "Behringer", "Apple", "Arturia", "Moog", "Marshall");
    $categories = array("Guitars", "Brass", "Percussion", "Synths", "Keys");
    $instruments = array("Jazzmaster", "Wangcaster", "Microfreak", "303", "DMX", "Modular");
    $lorem = "Lorem ipsum dolor sit amet consectetur adipiscing elit. Donec porttitor ante ac metus ornare, in mattis lorem efficitur. Nulla enim.";

    $head = "INSERT INTO products (name, price, category, description, stock) VALUES ";
    $values = "";

    for($i = 0; $i < 50; $i++) 
    {
        $a1 = rand(0, count($brands)-1);
        $a2 = rand(0, count($instruments)-1);
        $b = rand(0, 999999);
        $c = rand(0, count($categories)-1);

        $values = $values."ROW(
            '$brands[$a1] $instruments[$a2]',
            $b,
            '$categories[$c]',
            '$lorem',
            1
        ), ";
    }

    $values = $values."ROW(
        '$brands[$a1] $instruments[$a2]',
        $b,
        '$categories[$c]',
        '$lorem',
        1
    )";

    $query = $head.$values;
    var_dump($query);

    if($mysqli->query($query) === TRUE)
    {
        echo "5 Products succesfully added";
    } 
    else
    {
        echo "Error adding random products.".$mysqli->error;
    }
?>