<?php

    /**
    * function: gets requested entry from database
    * params: ref to mysqli-object $mysqli, int $id
    * returns: mysqli-result object
    */
    function getResult(&$mysqli, int $id)
    {
        $query = "SELECT * FROM products WHERE id=".$id;
        $result = $mysqli->query($query);

        if($result === FALSE)
        {
            echo '<p>Error retrieving data from database. '.$mysqli->error.'</p>';
        }
        else
        {
            return $result;
        }
    }

    /**
    * function: creates a blank MySQL update statement
    * params: ref to mysqli-object $mysqli, int $id
    * returns: mysqli-statement object
    */
    function sqlUpdate(&$mysqli, int $id)
    {
        $statement = $mysqli->prepare(
            "UPDATE products SET
            name = ?,
            price = ?,
            category = ?,
            description = ?,
            stock = ?
            WHERE id=".$id
        );

        if($statement === FALSE)
        {
            echo 'Error: '.$mysqli->error;
        }
        else
        {
            return $statement;
        }
    }

    /**
     * executes statement, checks for errors, and closes active connections
     * takes a reference to a statement object, a reference to a mysqli object and a product name to display
     * returns void
     */
    function execute(&$statement, &$mysqli, string $name)
    {
        if($statement->execute() === TRUE)
        {
            echo 'Success';
        } 
        else
        {
            echo 'Error updating the table. This was the error: '.$mysqli->error;
        }

        $statement->close();
    }