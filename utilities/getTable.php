<?php

    /**
     * 
     */
    function getResult(&$mysqli)
    {
        setlocale(LC_MONETARY, 'nl_NL');
        $query = "SELECT id, name, price, category, stock FROM products";
        $result = $mysqli->query($query);

        if($result === FALSE)
        {
            echo '<p>Error retrieving table from database. '.$mysqli->error.'</p>';
        }
        else
        {
            return $result;
        }
    }

    /**
     * Displays database entries in a table on screen
     * Takes data object from database
     * Returns void
     */
    function generateTable(&$result)
    {
        if($result->num_rows > 0)
        {
            while($row = $result->fetch_assoc())
            {
                $priceFloat = floatval($row['price'])/100;
                echo '
                    <tr>
                        <td>'.$row['id'].'</td>
                        <td>'.$row['name'].'</td>
                        <td>'.money_format('%#2n', $priceFloat).'</td>
                        <td>'.$row['category'].'</td>
                ';
                        toYesNo($row['stock']); // Converts int to yes/no
                echo '
                        <td class="buttoncell">
                            <a href="editForm.php?id='.$row['id'].'">
                                <div class="tbutton edit">Edit</div>
                            </a>
                        </td>
                        <td class="buttoncell">
                            <a href="deleteForm.php?id='.$row['id'].'">
                                <div class="tbutton delete">Delete</div>
                            </a>
                        </td>
                    </tr>
                ';
            }
        }
        else
        {
            echo 'No results';
        }
    }

    /**
     * Converts integer value to yes/no
     * Takes an integer
     * Returns "Yes" if integer is 1, "No" if it's not
     */
    function toYesNo(int $bit)
    {
        if($bit == 1)
        {
            echo '
                <td>Yes</td>';
        }
        else
        {
            echo '
                <td>No</td>';
        }
    }