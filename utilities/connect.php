<?php

   /**
   * function: opens connection to MySQL-database and creates mysqli-object
   * params: username, password
   * returns: mysqli-object
   */
   function connect(string $username, string $password)
   {
      $mysqli = new mysqli(
         'localhost', // of 127.0.0.1
         $username, // je database username
         $password, // je database user password
         'webshop' // je database naam
      );

      if ($mysqli->connect_error) {
         die('Connect Error (' . $mysqli->connect_errno . ') '
                  . $mysqli->connect_error);
      }

      // echo 'Success... ' . $mysqli->host_info . "\n";
      return $mysqli;
   }

      /**
      * function: closes connection to MySQL-database
      * params: ref to mysqli-object $mysqli
      * returns: void
      */
   function disconnect(&$mysqli)
   {
      $mysqli->close();
   }
   
?>