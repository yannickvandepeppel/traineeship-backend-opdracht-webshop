<?php 

    /**
     * prepares mysqli statement object
     * takes reference to mysqli object
     * returns statement object when successful, void when unsuccessful
     */
    function initialize(&$mysqli)
    {
        $statement = $mysqli->prepare(
            "INSERT INTO products (
            name,
            price,
            category,
            description,
            stock
            ) VALUES (?, ?, ?, ?, ?)");

        if($statement === FALSE)
        {
            echo 'Error: '.$mysqli->error;
        }
        else
        {
            return $statement;
        }
    }
    
    /**
     * executes statement, checks for errors, and closes active connections
     * takes a reference to a statement object, a reference to a mysqli object
     * returns void
     */
    function execute(&$statement, &$mysqli)
    {
        if($statement->execute() === TRUE)
        {
            echo  'Success';
        } 
        else
        {
            echo 'Error adding to table. This was the error: '.$mysqli->error;
        }

        $statement->close();
    }