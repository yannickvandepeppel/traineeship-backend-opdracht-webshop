<?php

    /**
    * function: generates correct feedback message depending on user action
    * params: $msgId (string)
    * returns: string
    */
    function messages(string $msgId)
    {
        switch($msgId)
        {
            case 'add':
                return 'Product has been added successfully';
            case 'edit':
                return 'Product has been updated successfully';
            case 'delete':
                return 'Product has been removed from database';
        }
    }