<?php

    /**
    * function: Generates mysqli-statement to create a user in the database
    * params: ref to mysqli-object
    * returns: mysqli statement-object
    */
    function register(&$mysqli)
    {
        $statement = $mysqli->prepare(
            "INSERT INTO users (
                name,
                email,
                password
            ) VALUES (?, ?, ?)"
        );

        if($statement === FALSE)
        {
            echo 'Error: '.$mysqli->error;
        }
        else
        {
            return $statement;
        }
    }

    /**
    * function: takes a mysqli statement and executes it on the database
    * params: refs to mysqli statement object and mysqli-object
    * returns: void
    */
    function execute(&$statement, &$mysqli)
    {
        if($statement->execute() === TRUE)
        {
            echo  'Success';
        } 
        else
        {
            echo 'Error adding to table. This was the error: '.$mysqli->error;
            die();
        }

        $statement->close();
    }

    /**
    * function: Checks email against the emails in the DB, returns false if none are found, else returns true
    * params: ref to mysqli-object, email string
    * returns: boolean
    */
    function isEmailUnique(&$mysqli, string $email)
    {
        $query = "SELECT email FROM users WHERE email='".$email."'";

        $result = $mysqli->query($query);

        if($result === FALSE)
        {
            echo 'Error: '.$mysqli->error;
            die();
        }

        if($result->num_rows > 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    /**
    * function: checks email against the database and returns the associated user ID
    * params: ref to mysqli-object, email string
    * returns: integer
    */
    function getId(&$mysqli, string $email) 
    {
        $query = "SELECT id FROM users WHERE email='".$email."'";

        $result = $mysqli->query($query);

        if($result === FALSE)
        {
            echo 'Error: '.$mysqli->error;
            die();
        }
        else
        {
            $row = $result->fetch_assoc();
            return $row['id'];
        }
    }

    /**
    * function: gets the hashed password from the database associated with a user ID
    * params: Ref to mysqli-object , user ID integer
    * returns: string
    */  
    function getPassword(&$mysqli, int $id) 
    {
        
        $query = "SELECT password FROM users WHERE id=".$id;
        
        $result = $mysqli->query($query);
        
        if($result === FALSE)
        {
            echo 'Error: '.$mysqli->error;
            die();
        }
        else
        {
            $row = $result->fetch_assoc();
            return $row['password'];
        }
    }

    /**
    * function: gets user data associated with provided user ID
    * params: ref to mysqli-object, user ID integer
    * returns: associated array with fields 'name' (string), 'email' (string) and 'isadmin' (integer, either '0' or '1')
    */
    function getUser(&$mysqli, int $id) 
    {
        $query = "SELECT name, email, isadmin FROM users WHERE id=".$id;;
        $result = $mysqli->query($query);

        if($result === FALSE)
        {
            echo 'Error: '.$mysqli->error;
            die();
        }
        else
        {
            $row = $result->fetch_assoc();
            return $row;
        }
    }