<?php

    /**
    * function: deletes entry from database
    * params: ref to mysqli-object $mysqli, integer $id
    * returns: mysqli statement-object
    */
    function sqlDelete(&$mysqli, int $id)
    {
        $statement = $mysqli->prepare(
            "DELETE FROM products WHERE id=".$id);

        if($statement === FALSE)
        {
            echo 'Error: '.$mysqli->error;
        }
        else
        {
            return $statement;
        }
    }

        /**
     * executes statement, checks for errors, and closes active connections
     * takes a reference to a statement object, a reference to a mysqli object and a product name to display
     * returns void
     */
    function execute(&$statement, &$mysqli)
    {
        if($statement->execute() === TRUE)
        {
            echo 'Success';
        } 
        else
        {
            echo 'Error adding to table. This was the error: '.$mysqli->error;
        }

        $statement->close();
    }