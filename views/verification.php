<?php
    require_once '/var/www/webshop/utilities/connect.php';
    require_once '/var/www/webshop/utilities/auth.php';
    session_start();

        // Checks if user has entered credentials, returns a 403 if they're not
        if(!$_SESSION['loggedin'])
        {
            header('HTTP/1.0 403 Forbidden');
            die();
        }

        $mysqli = connect('verifyuser', 'watzijnwijaanhetdoenmeneer');
        $id = $_SESSION['id'];
        $password = $_SESSION['password'];
        $hash = getPassword($mysqli, $id);

        if(password_verify($password, $hash))
        {
            $user = getUser($mysqli, $id);
            $_SESSION['verified'] = true;
            $_SESSION['name'] = $user['name'];
            $_SESSION['email'] = $user['email'];
            $_SESSION['isadmin'] = $user['isadmin'];

            disconnect($mysqli);
            header('Location: https://webshop.yannick.xpdev.nl/views/productTable.php');
            die();
        }
        else
        {
            header('Location: https://webshop.yannick.xpdev.nl/views/login.php?validated=false');
            die();
        }
?>

<p>Verifying...</p>