<?php 
    if(isset($_POST['submit']))
    {
        require_once '/var/www/webshop/utilities/connect.php';
        require_once '/var/www/webshop/utilities/auth.php';

        $name = htmlspecialchars($_POST['name']);
        $email = htmlspecialchars($_POST['email']);
        $password = $_POST['password'];
        $passwordhash = htmlspecialchars(password_hash($password, PASSWORD_DEFAULT));

        $mysqli = connect('newuser', 'ikbennieuwhier');

        if(!isEmailUnique($mysqli, $email))
        {
            header('Location: https://webshop.yannick.xpdev.nl/views/register.php?unique=false');
            die();
        }

        $statement = register($mysqli);
        $statement->bind_param('sss', $name, $email, $passwordhash);
        execute($statement, $mysqli);
        
        session_start();

        $_SESSION['id'] = getId($mysqli, $email);
        $_SESSION['password'] = $password;
        $_SESSION['loggedin'] = true;

        var_dump($_SESSION['id']);

        disconnect(($mysqli));
        header('Location: https://webshop.yannick.xpdev.nl/views/verification.php');
        die();
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <div class="container">
        <div class="form">
            <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST">
                <input 
                    type="text" 
                    name="name" 
                    id="name" 
                    required>
                <input 
                    type="email" 
                    name="email" 
                    id="email" 
                    required>
                <input 
                    type="password" 
                    name="password" 
                    id="password" 
                    required>
                <input 
                    type="submit" 
                    name="submit" 
                    id="submit" 
                    value="Register">
            </form>
        </div>
    </div>
</body>
</html>