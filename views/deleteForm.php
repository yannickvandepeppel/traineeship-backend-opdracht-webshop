<?php
    if(isset($_POST['submit']))
    {
        if($_POST['confirm'] == 1)
        {
            require_once '/var/www/webshop/utilities/deleteProduct.php';
            require_once '/var/www/webshop/utilities/connect.php';

            $id = $_POST['id'];
            $mysqli = connect('webshopuser', 'hallohallohallo');
            $statement = sqlDelete($mysqli, $id);

            execute($statement, $mysqli);

            disconnect($mysqli);
            
            // Redirects to product overview page
            header('Location: https://webshop.yannick.xpdev.nl/views/productTable.php?msgid=delete');
            die();
        }
        else
        {
            header("Location: https://webshop.yannick.xpdev.nl/view/productTable.php");
            die();
        }
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Delete Product</title>
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <div class="container">
        <div class="form">

            <h2>Are you sure you want to delete this product?</h2>
            <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST">
                <label for=" yes">Yes</label>
                <input type="radio" name="confirm" id="yes" value="1" required>
                <label for="no">No</label>
                <input type="radio" name="confirm" id="no" value="0">
                <input type="hidden" name="id" id="id" value="<?php echo $_GET['id']; ?>">
                <input type="submit" name="submit" id="submit" value="Confirm">

            </form>
        </div>
    </div>
</body>

</html>