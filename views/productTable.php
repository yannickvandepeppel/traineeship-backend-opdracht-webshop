<?php
    session_start();
    require_once '/var/www/webshop/utilities/getTable.php';
    require_once '/var/www/webshop/utilities/connect.php';
    require_once '/var/www/webshop/utilities/messages.php';
    $mysqli = connect('webshopuser', 'hallohallohallo');
    $result = getResult($mysqli);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Products</title>
    <link rel="stylesheet" href="css/style.css">
</head>
    <body>
        <div class="container">

            <!-- Displays a message if user interacted with the database -->
            <?php if(isset($_GET['msgid'])): ?>
                <div class="confirmation">
                    <p><?php echo messages($_GET['msgid'])?></p>
                </div>
            <?php endif;?>

            <p>Create a new product <a href="productForm.php">here</a></p>
            <p>Hi <?php echo $_SESSION['name']?>!</p>

            <h1>Content</h1>
            <table>
                <thead>
                    <tr>
                        <th>Item no.</th>
                        <th>Product Name</th>
                        <th>Price</th>
                        <th>Category</th>
                        <th>In stock</th>
                    </tr>
                </thead>
                <tbody>
                    <?php generateTable($result);?>
                </tbody>
                <tfoot>
                    <tr>
                        <td><strong>Total:</strong></td>
                        <?php echo '<td>'.$result->num_rows.'</td>';?>
                    </tr>
                </tfoot>
            </table>
        </div>
    </body>
</html>

<?php disconnect($mysqli);?>