<?php
    
    if(isset($_POST['submit']))
    {
        session_start();

        $_SESSION['email'] = $_POST['email'];
        $_SESSION['password'] = $_POST['password'];
        $_SESSION['loggedin'] = true;

        header('Location: https://webshop.yannick.xpdev.nl/views/verification.php');
        die();
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <div class="container">
        <div class="form">
            <h2>Please login below</h2>
            <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST">
                <input 
                    type="email" 
                    name="email" 
                    id="email" 
                    required>
                <input 
                    type="password" 
                    name="password" 
                    id="password" 
                    required>
                <input 
                    type="submit" 
                    name="submit" 
                    id="submit" 
                    value="Register">
            </form>
        </div>
    </div>
</body>
</html>