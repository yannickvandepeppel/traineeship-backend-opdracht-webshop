<?php
    require_once '/var/www/webshop/utilities/connect.php';
    require_once '/var/www/webshop/utilities/editProduct.php';

    $mysqli = connect('webshopuser', 'hallohallohallo');
    $id = $_GET['id'];

    // Update database on form submission
    if(isset($_POST['submit']))
    {
        $id = $_POST['id']; // Updates id variable to the one sent with the post request to fetch correct query from database

        $statement = sqlUpdate($mysqli, $id);
        $statement->bind_param('sissi', $name, $price, $category, $description, $stock);

        $name = htmlspecialchars($_POST['name']);
        $price = htmlspecialchars($_POST['price']);
        $category = htmlspecialchars($_POST['category']);
        $description = htmlspecialchars($_POST['description']);
        $stock = htmlspecialchars($_POST['stock']);

        execute($statement, $mysqli, $name);

        // Redirects to product overview page
        header('Location: https://webshop.yannick.xpdev.nl/views/productTable.php?msgid=edit');
        die();
    }

    // Gets database entry for display in form
    $result = getResult($mysqli, $id);
    $row = $result->fetch_assoc();

    disconnect($mysqli);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Product</title>
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
<div class="container">
    <div class="form">
        <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST">
            <label for="name">Name</label>
            <input 
                type="text" 
                name="name" 
                id="name" 
                maxlength="100" 
                value="<?php echo $row['name']; ?>"
                required>
            <br>

            <label for=" price">Price</label>
            <input 
                type="number" 
                name="price" 
                id="price" 
                min="0" 
                max="9999999" 
                value="<?php echo $row['price']; ?>"
                required>
            <br>

            <label for=" category">Category</label>
            <select 
                name="category" 
                id="category"
                value="<?php echo $row['category']; ?>">
                <option value="Guitars">Guitars</option>
                <option value="Brass">Brass</option>
                <option value="Percussion">Percussion</option>
                <option value="Synths">Synths</option>
                <option value="Keys">Keys</option>
            </select>
            <br>

            <label for="description">Description</label>
            <input 
                type="text" 
                name="description" 
                id="description"
                maxlength="65535"
                value="<?php echo $row['description']; ?>" 
                required>
            <br>

            <p>In Stock:</p><br>
            <label for=" yes">Yes</label>
            <input 
                type="radio" 
                name="stock" 
                id="yes" 
                value="1" 
                <?php echo ($row['stock'] == "1") ? ('checked') : ('');?>
                required>
            <label for="no">No</label>
            <input 
                type="radio" 
                name="stock" 
                id="no" 
                value="0"
                <?php echo ($row['stock'] == "0") ? ('checked') : ('');?>>
            <input 
                type="submit" 
                name="submit" 
                id="submit" 
                value="Edit Product">
            <input 
                type="hidden" 
                name="id" 
                id="id" 
                value="<?php echo $_GET['id']; ?>">
        </form>
    </div>
</div>
</body>

</html>