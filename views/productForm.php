<?php
    // Executes SQL query once a valid post request is sent
    if(isset($_POST['submit']))
    {
        require_once '/var/www/webshop/utilities/connect.php';
        require_once '/var/www/webshop/utilities/addProduct.php';

        $mysqli = connect('webshopuser', 'hallohallohallo');
        $statement = initialize($mysqli);

        $statement->bind_param('sissi', $name, $price, $category, $description, $stock);

        $name = htmlspecialchars($_POST['name']);
        $price = htmlspecialchars($_POST['price']);
        $category = htmlspecialchars($_POST['category']);
        $description = htmlspecialchars($_POST['description']);
        $stock = htmlspecialchars($_POST['stock']);

        execute($statement, $mysqli);

        disconnect($mysqli);
        
        // Redirects to product overview page
        header('Location: https://webshop.yannick.xpdev.nl/views/productTable.php?msgid=add');
        die();
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add product</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

    <div class="container">
        <div class="form">


            <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" maxlength="100" required><br>

                <label for=" price">Price</label>
                <input type="number" name="price" id="price" min="0" max="9999999" required><br>

                <label for=" category">Category</label>
                <select name="category" id="category">
                    <option value="Guitars">Guitars</option>
                    <option value="Brass">Brass</option>
                    <option value="Percussion">Percussion</option>
                    <option value="Synths">Synths</option>
                    <option value="Keys">Keys</option>
                </select><br>

                <label for="description">Description</label>
                <input type="text" name="description" id="description" maxlength="65535" required><br>

                <p>In Stock:</p><br>
                <label for=" yes">Yes</label>
                <input type="radio" name="stock" id="yes" value="1" required>
                <label for="no">No</label>
                <input type="radio" name="stock" id="no" value="0">
                <input type="submit" name="submit" id="submit" value="Add Product">
            </form>
        </div>
    </div>

</body>
</html>